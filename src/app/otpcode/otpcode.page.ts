import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-otpcode',
  templateUrl: './otpcode.page.html',
  styleUrls: ['./otpcode.page.scss'],
})
export class OTPCodePage implements OnInit {
  public UserPhoneNumber = '+39 123456789'
  public authCode1 = ''
  public authCode2 = ''
  public authCode3 = ''
  public authCode4 = ''

  constructor(
    private router: Router,
    private navCtrl: NavController,
  ) { }

  ngOnInit() {
  }

  btnVerifyCodeTapped() {

  }
  resendCodeTapped() {

  }
  btnBackTapped() {
    this.navCtrl.goBack(true)

  }
}
