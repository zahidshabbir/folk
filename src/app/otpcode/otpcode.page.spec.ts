import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OTPCodePage } from './otpcode.page';

describe('OTPCodePage', () => {
  let component: OTPCodePage;
  let fixture: ComponentFixture<OTPCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OTPCodePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OTPCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
