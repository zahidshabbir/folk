import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'OTPSend', pathMatch: 'full' },
  { path: 'OTPSend', loadChildren: './otpsend/otpsend.module#OTPSendPageModule' },
  { path: 'OTPCode', loadChildren: './otpcode/otpcode.module#OTPCodePageModule' },
  { path: 'SplashScreen', loadChildren: './splash-screen/splash-screen.module#SplashScreenPageModule' },
  { path: 'Intro', loadChildren: './intro/intro.module#IntroPageModule' },
  { path: 'test', loadChildren: './test/test.module#TestPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
