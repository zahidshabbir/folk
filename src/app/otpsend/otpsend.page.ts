import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-otpsend',
  templateUrl: './otpsend.page.html',
  styleUrls: ['./otpsend.page.scss'],
})
export class OTPSendPage implements OnInit {
  public phoneNumber = '1234567890'
  public codes = ['+39','+40','+41']
  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }
  btnSendOtpTapped() {
    
  }
  btnBackTapped() {
    this.navCtrl.goBack(true)

  }
}
