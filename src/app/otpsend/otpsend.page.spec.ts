import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OTPSendPage } from './otpsend.page';

describe('OTPSendPage', () => {
  let component: OTPSendPage;
  let fixture: ComponentFixture<OTPSendPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OTPSendPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OTPSendPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
